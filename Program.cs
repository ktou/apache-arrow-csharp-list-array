﻿using System;
using System.IO;
using System.Threading.Tasks;

namespace ApacheArrowCSharpListArray
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var offsetsBuilder = new Apache.Arrow.Int32Array.Builder();
            var valuesBuilder = new Apache.Arrow.StringArray.Builder();
            int nElements = 0;
            int nValues = 0;
            offsetsBuilder.Append(nValues); nElements++;
            valuesBuilder.Append("Hello"); nValues++;
            valuesBuilder.Append("World"); nValues++;
            offsetsBuilder.Append(nValues); nElements++;
            valuesBuilder.Append("A"); nValues++;
            valuesBuilder.Append("B"); nValues++;
            valuesBuilder.Append("C"); nValues++;
            var offsets = offsetsBuilder.Build();
            var values = valuesBuilder.Build();
            var valueDataType = values.Data.DataType;
            var listValueField = new Apache.Arrow.Field("item",
                                                        valueDataType,
                                                        true);
            var listDataType = new Apache.Arrow.Types.ListType(listValueField,
                                                               valueDataType);
            var listArray = new Apache.Arrow.ListArray(listDataType,
                                                       nElements,
                                                       offsets.Data.Buffers[1],
                                                       values,
                                                       offsets.Data.Buffers[0],
                                                       offsets.Data.NullCount,
                                                       offsets.Data.Offset);
            var listField = new Apache.Arrow.Field("string-list",
                                                   listDataType,
                                                   true);
            var schema = new Apache.Arrow.Schema(new [] {listField}, null);
            var recordBatch = new Apache.Arrow.RecordBatch(schema,
                                                           new [] {listArray},
                                                           listArray.Data.Length);
            using (var output = new FileStream("string-list.arrow",
                                               FileMode.Create))
            using (var writer = new Apache.Arrow.Ipc.ArrowFileWriter(output, schema))
            {
                await writer.WriteRecordBatchAsync(recordBatch);
                await writer.WriteEndAsync();
            }
        }
    }
}
